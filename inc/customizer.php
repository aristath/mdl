<?php
/**
 * MDL Theme Customizer
 *
 * @package MDL
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function mdl_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'mdl_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function mdl_customize_preview_js() {
	wp_enqueue_script( 'mdl_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'mdl_customize_preview_js' );

/**
 * Kirki time.
 * Do not proceed any further if we're unable to find the Kirki class.
 */
if ( ! class_exists( 'Kirki' ) ) {
	// return;
}

/**
 * Returns an array of the MDL colors.
 * See http://www.getmdl.io/customize/index.html for details
 *
 * @return array
 */
function mdl_theme_colors_array() {

	$colors = array(
		'cyan'        => __( 'Cyan', 'mdl' ),
		'light_green' => __( 'Light Green', 'mdl' ),
		'amber'       => __( 'Amber', 'mdl' ),
		'orange'      => __( 'Orange', 'mdl' ),
		'brown'       => __( 'Brown', 'mdl' ),
		'grey'        => __( 'Grey', 'mdl' ),
		'purple'      => __( 'Purple', 'mdl' ),
		'deep_purple' => __( 'Deep Purple', 'mdl' ),
		'blue'        => __( 'Blue', 'mdl' ),
		'light_blue'  => __( 'Light Blue', 'mdl' ),
		'indigo'      => __( 'Indigo', 'mdl' ),
		'teal'        => __( 'Teal', 'mdl' ),
		'green'       => __( 'Green', 'mdl' ),
		'lime'        => __( 'Lime', 'mdl' ),
		'yellow'      => __( 'Yellow', 'mdl' ),
		'red'         => __( 'Red', 'mdl' ),
		'pink'        => __( 'Pink', 'mdl' ),
		'blue_grey'   => __( 'Blue Grey', 'mdl' ),
		'deep_orange' => __( 'Deep Orange', 'mdl' ),
	);

	return $colors;

}
/**
 * Add the configuration for the theme.
 * We'll use serialized options.
 */
Kirki::add_config( 'mdl', array(
	'capability'    => 'edit_theme_options',
    'option_type'   => 'theme_mod',
) );

Kirki::add_field( 'mdl', array(
	'settings' => 'mdl_primary_color',
    'label'    => __( 'Primary Color', 'mdl' ),
    'section'  => 'colors',
    'type'     => 'select',
    'priority' => 10,
    'default'  => 'blue_grey',
	'choices'  => mdl_theme_colors_array(),
	'active_callback' => '__return_true',
) );

Kirki::add_field( 'mdl', array(
	'settings' => 'mdl_secondary_color',
    'label'    => __( 'Secondary Color', 'mdl' ),
    'section'  => 'colors',
    'type'     => 'select',
    'priority' => 10,
    'default'  => 'blue_grey',
	'choices'  => mdl_theme_colors_array(),
	'active_callback' => '__return_true'
) );
