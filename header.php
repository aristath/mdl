<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package MDL
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="mdl-layout mdl-js-layout mdl-layout--fixed-header hfeed site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mdl' ); ?></a>

		<header id="masthead" class="android-header mdl-layout__header mdl-layout__header--waterfall site-header" role="banner">
			<div class="mdl-layout__header-row">

				<span class="site-branding android-title mdl-layout-title">
					<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
					<p class="site-description"><?php bloginfo( 'description' ); ?></p>
				</span><!-- .site-branding -->

				<!-- Add spacer, to align navigation to the right in desktop -->
				<div class="android-header-spacer mdl-layout-spacer"></div>

				<div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
					<label class="mdl-button mdl-js-button mdl-button--icon" for="search-field">
						<i class="material-icons">search</i>
					</label>
					<div class="mdl-textfield__expandable-holder">
						<input class="mdl-textfield__input" type="text" id="search-field" />
					</div>
				</div>
				<!-- Navigation -->
				<div class="android-navigation-container">
					<nav class="android-navigation mdl-navigation">
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">Phones</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">Tablets</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">Wear</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">TV</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">Auto</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">One</a>
						<a class="mdl-navigation__link mdl-typography--text-uppercase" href="">Play</a>
					</nav>
				</div>
				<button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
					<i class="material-icons">more_vert</i>
				</button>
				<ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="more-button">
					<li class="mdl-menu__item">5.0 Lollipop</li>
					<li class="mdl-menu__item">4.4 KitKat</li>
					<li disabled class="mdl-menu__item">4.3 Jelly Bean</li>
					<li class="mdl-menu__item">Android History</li>
				</ul>
				<span class="android-mobile-title mdl-layout-title">
					<!-- <img class="android-logo-image" src="images/android-logo.png"> -->
				</span>
			</div>
		</header>

<!-- <nav id="site-navigation" class="main-navigation" role="navigation">
<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'mdl' ); ?></button>
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
</nav>
</header> -->

	<div id="content" class="site-content">
